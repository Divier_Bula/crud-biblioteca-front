import { Component, OnInit } from '@angular/core';
import { AutorDTO } from '../DTO/AutorDTO';
import { isNullOrUndefined } from 'util';
import { LibroDTO } from '../DTO/LibroDTO';

@Component({
  selector: 'app-autor',
  templateUrl: './autor.component.html',
  styleUrls: ['./autor.component.css']
})
export class AutorComponent implements OnInit {
  ngOnInit(){
    this.ConsultarAutores();
  }
  title  : string;
  nombreButton  : string;
  selectedAutor : AutorDTO = new AutorDTO();
  responseLibroArray: LibroDTO[] = [];
  AutorArray: AutorDTO[] = [];

  constructor() {
    this.title = 'Page_Autor';
    this.nombreButton = 'Crear';
  }
   
  ConsultarAutores()
  {
    debugger
// this.httpFactory.getObjParamsHeader('nombremetodo',"").then((resp) => {
    //   debugger;

    //   //Se realiza el llanado de la entidad LibroDTO();
      
    // }).catch((err) => {
    //   console.log(err);
    // });
    
    var autorDTO = new AutorDTO();
    autorDTO._cod_Autor=1; autorDTO._nombre_Autor='Juan'; autorDTO._apellido_Autor= 'Mora';
    this.AutorArray.push(autorDTO);
    var autorDTO = new AutorDTO();
    autorDTO._cod_Autor=2; autorDTO._nombre_Autor='Maria'; autorDTO._apellido_Autor= 'Sanchez';
    this.AutorArray.push(autorDTO);
  }

  CrearYActualizarAutor(selectedAutor:AutorDTO){
    if ( !isNullOrUndefined(this.selectedAutor._nombre_Autor) &&  !isNullOrUndefined(this.selectedAutor._apellido_Autor) && this.selectedAutor._nombre_Autor!= "" &&  this.selectedAutor._apellido_Autor != "")
    {
      if(this.selectedAutor._cod_Autor==0)
      {
        alert("llama Api crear");
        // this.httpFactory.postHeaderService('NAME',"").then((resp) => {
        //   debugger;
        //   //Se realiza el llanado de la entidad LibroDTO();
        // }).catch((err) => {
        //   console.log(err);
        // });
        this.selectedAutor._cod_Autor = this.AutorArray.length > 0 ? this.AutorArray[this.AutorArray.length-1]._cod_Autor+1 : 1;
        this.AutorArray.push(this.selectedAutor);
      }
      else{
        alert("llama Api actualizar");
        // this.httpFactory.postHeaderService('NAME',"").then((resp) => {
        //   debugger;
        //   //Se realiza el llanado de la entidad LibroDTO();
        // }).catch((err) => {
        //   console.log(err);
        // });
      }
      this.selectedAutor = new AutorDTO();
      this.nombreButton = 'Crear';
    }else{
      alert('Porfavor ingresar todos los datos');
    }
  }

   ActualizarAutor(item: AutorDTO){
    this.selectedAutor = item;
    this.nombreButton = 'Actualizar';
  }

  EliminarAutor(item : AutorDTO){
    if(confirm('Estas seguro de elimiar el autor?'))
    alert("llama Api eliminar");
  // this.httpFactory.postHeaderService('NAME',"").then((resp) => {
          //   debugger;
          //   //Se realiza el llanado de la entidad LibroDTO();
          // }).catch((err) => {
          //   console.log(err);
          // });

    this.selectedAutor = item
    this.AutorArray = this.AutorArray.filter(x => x!= this.selectedAutor)
    this.selectedAutor = new AutorDTO();
  }

  ConsultarLibro(name : string){
    
    alert("llama Api consulta por libro");
        // this.httpFactory.postHeaderService('NAME',"").then((resp) => {
        //   debugger;
        //   //Se realiza el llanado de la entidad LibroDTO();
        // }).catch((err) => {
        //   console.log(err);
        // });

        var libroDTO = new LibroDTO();
        libroDTO._cod_Libro = 1;
        libroDTO._nombre_Libro = 'libro1';
        libroDTO._iSBN = 123;
        libroDTO._cod_Autor = 1;
        libroDTO._cod_Categoria = 2;
        this.responseLibroArray.push(libroDTO);
    
        var libroDTO = new LibroDTO();
        libroDTO._cod_Libro = 2;
        libroDTO._nombre_Libro = 'libro2';
        libroDTO._iSBN = 123;
        libroDTO._cod_Autor = 1;
        libroDTO._cod_Categoria = 2;
        this.responseLibroArray.push(libroDTO);

  }

  Cancelar()
  {
    this.selectedAutor = new AutorDTO();
    this.nombreButton = 'Crear';
  }

}