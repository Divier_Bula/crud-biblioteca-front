import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpFactoryService {
  APIEndpoint = "http://localhot/api";
  constructor(private _http: HttpClient) { }
  
  post(endpoint, data) {
    return new Promise((resolve, reject) => {
      this._http.post(this.APIEndpoint + endpoint, data)
      .subscribe((results) => {
        resolve(results);
      }, (error) => {
        reject(error);
      });
    });
  }
  getObjParamsHeader(endpoint, data) {
    const header = new HttpHeaders({
      'Accept' : 'application/pdf',
      "Content-Type": "application/octet-stream"
    });
    return new Promise((resolve, reject) => {
      this._http.get(this.APIEndpoint + endpoint, {
        params: data,
        headers: header,
        responseType: 'blob' as 'json'
      })
      .subscribe((results) => {
        resolve(results);
      }, (error) => {
        reject(error);
      });
    });
  }
  postHeaderService(endpoint, data) {
    const header = new HttpHeaders({
      'Content-Type' : 'application/json',
      'Accept' : 'q=0.8;application/json;q=0.9'
    });
    return new Promise((resolve, reject) => {
      this._http.post(this.APIEndpoint + endpoint, data, {
        headers: header
      })
      .subscribe((results) => {
        resolve(results);
      }, (error) => {
        reject(error);
      });
    });
  }
  get(endpoint, data) {
    return new Promise((resolve, reject) => {
      this._http.get(this.APIEndpoint + endpoint + data)
      .subscribe((results) => {
        resolve(results);
      }, (error) => {
        reject(error);
      });
    });
  }
}
