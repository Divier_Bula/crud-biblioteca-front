import { Component, OnInit } from '@angular/core';
import { CategoriaDTO } from '../DTO/CategoriaDTO';
import { isNullOrUndefined } from 'util';
import { LibroDTO } from '../DTO/LibroDTO';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {
  ngOnInit(){
    this.ConsultarCategorias();
  }
  title  : string;
  nombreButton  : string;
  selectedCategoria : CategoriaDTO = new CategoriaDTO();
  responseLibroArray: LibroDTO[] = [];

  CategoriaArray: CategoriaDTO[] = [];

  constructor() {
    this.title = 'Page_Categoria';
    this.nombreButton = 'Crear';
  }

  ConsultarCategorias() {
    
   // this.httpFactory.getObjParamsHeader('nombremetodo',"").then((resp) => {
    //   debugger;

    //   //Se realiza el llanado de la entidad LibroDTO();
      
    // }).catch((err) => {
    //   console.log(err);
    // });

    var categoriaDTO = new CategoriaDTO();
    categoriaDTO._cod_Categoria=1; categoriaDTO._nombre_Categoria='Drama'; categoriaDTO._descripcion_Categoria= 'asdasdasdasdasd';
    this.CategoriaArray.push(categoriaDTO);
    var categoriaDTO = new CategoriaDTO();
    categoriaDTO._cod_Categoria=2; categoriaDTO._nombre_Categoria='Arte'; categoriaDTO._descripcion_Categoria= 'asdsadasdasdsadasd';
    this.CategoriaArray.push(categoriaDTO);
  }

  CrearYActualizarCategoria(selectedCategoria:CategoriaDTO){
    
    if ( !isNullOrUndefined(this.selectedCategoria._nombre_Categoria) &&  !isNullOrUndefined(this.selectedCategoria._descripcion_Categoria) && this.selectedCategoria._nombre_Categoria!= "" &&  this.selectedCategoria._descripcion_Categoria != "")
    {
      if(this.selectedCategoria._cod_Categoria==0)
      {
        alert("llama Api crear");
        // this.httpFactory.postHeaderService('NAME',"").then((resp) => {
        //   debugger;
        //   //Se realiza el llanado de la entidad LibroDTO();
        // }).catch((err) => {
        //   console.log(err);
        // });

        this.selectedCategoria._cod_Categoria = this.CategoriaArray.length > 0 ? this.CategoriaArray[this.CategoriaArray.length-1]._cod_Categoria+1 : 1;
        this.CategoriaArray.push(this.selectedCategoria);
      }
      else{
        alert("llama Api actualizar");
        // this.httpFactory.postHeaderService('NAME',"").then((resp) => {
        //   debugger;
        //   //Se realiza el llanado de la entidad LibroDTO();
        // }).catch((err) => {
        //   console.log(err);
        // });
      }
      this.selectedCategoria = new CategoriaDTO();
      this.nombreButton = 'Crear';
    }else{
      alert('Porfavor ingresar todos los datos');
    }
  }

   ActualizarCategoria(item: CategoriaDTO){
    this.selectedCategoria = item;
    this.nombreButton = 'Actualizar';
  }

  EliminarCategoria(item : CategoriaDTO){
    if(confirm('Estas seguro de elimiar la categoria?'))
    alert("llama Api eliminar");
    // this.httpFactory.postHeaderService('NAME',"").then((resp) => {
        //   debugger;
        //   //Se realiza el llanado de la entidad LibroDTO();
        // }).catch((err) => {
        //   console.log(err);
        // });

    this.selectedCategoria = item
    this.CategoriaArray = this.CategoriaArray.filter(x => x!= this.selectedCategoria)
    this.selectedCategoria = new CategoriaDTO();
  }

  ConsultarLibro(name : string){
    
    alert("llama Api consulta por libro");
        // this.httpFactory.postHeaderService('NAME',"").then((resp) => {
        //   debugger;
        //   //Se realiza el llanado de la entidad LibroDTO();
        // }).catch((err) => {
        //   console.log(err);
        // });

        var libroDTO = new LibroDTO();
        libroDTO._cod_Libro = 1;
        libroDTO._nombre_Libro = 'libro1';
        libroDTO._iSBN = 123;
        libroDTO._cod_Autor = 1;
        libroDTO._cod_Categoria = 2;
        this.responseLibroArray.push(libroDTO);
    
        var libroDTO = new LibroDTO();
        libroDTO._cod_Libro = 2;
        libroDTO._nombre_Libro = 'libro2';
        libroDTO._iSBN = 123;
        libroDTO._cod_Autor = 1;
        libroDTO._cod_Categoria = 2;
        this.responseLibroArray.push(libroDTO);

  }

}


