import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LibroComponent } from './libro/libro.component';
import { AutorComponent } from './autor/autor.component';
import { CategoriaComponent } from './categoria/categoria.component';


const routes: Routes = [{
  path: 'libro',
  component: LibroComponent
},
{
  path: 'autor',
  component: AutorComponent
},
{
  path: 'categoria',
  component: CategoriaComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
