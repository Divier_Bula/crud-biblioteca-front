export class LibroDTO {

    _cod_Libro : number = 0;

    _nombre_Libro: string;

    _iSBN: number;

    _cod_Autor: number;

    _cod_Categoria: number;
}
