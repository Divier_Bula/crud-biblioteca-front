import { Component, OnInit } from '@angular/core';
import { LibroDTO } from '../DTO/LibroDTO';
import { isNullOrUndefined } from 'util';
import { HttpFactoryService } from '../services/http-factory.service';

@Component({
  selector: 'app-libro',
  templateUrl: './libro.component.html',
  styleUrls: ['./libro.component.css']
})
export class LibroComponent implements OnInit {
  ngOnInit(){
    this.ConsultarLibros();
  }

  title  : string;
  nombreButton  : string;
  selectedLibro : LibroDTO = new LibroDTO();
  LibroArray: LibroDTO[] = [];
  responseLibroArray: LibroDTO[] = [];

  constructor(private httpFactory: HttpFactoryService) { 
    this.title = 'Page_Libro';
    this.nombreButton = 'Crear';
  }

  ConsultarLibros(){
    // this.httpFactory.getObjParamsHeader('nombremetodo',"").then((resp) => {
    //   debugger;

    //   //Se realiza el llanado de la entidad LibroDTO();
      
    // }).catch((err) => {
    //   console.log(err);
    // });
    
    var libroDTO = new LibroDTO();
    libroDTO._cod_Libro = 1;
    libroDTO._nombre_Libro = 'libro1';
    libroDTO._iSBN = 123;
    libroDTO._cod_Autor = 1;
    libroDTO._cod_Categoria = 2;
    this.LibroArray.push(libroDTO);

    var libroDTO = new LibroDTO();
    libroDTO._cod_Libro = 2;
    libroDTO._nombre_Libro = 'libro2';
    libroDTO._iSBN = 123;
    libroDTO._cod_Autor = 1;
    libroDTO._cod_Categoria = 2;
    this.LibroArray.push(libroDTO);
    
  }

  CrearYActualizarLibro(selectedLibro:LibroDTO){
     
    
    if ( !isNullOrUndefined(this.selectedLibro._nombre_Libro) &&  !isNullOrUndefined(this.selectedLibro._iSBN) &&  !isNullOrUndefined(this.selectedLibro._cod_Autor) &&  !isNullOrUndefined(this.selectedLibro._cod_Categoria) 
    && this.selectedLibro._nombre_Libro!= "")
    {
      if(this.selectedLibro._cod_Libro==0)
      {
        alert("llama Api crear");
        // this.httpFactory.postHeaderService('NAME',"").then((resp) => {
        //   debugger;
        //   //Se realiza el llanado de la entidad LibroDTO();
        // }).catch((err) => {
        //   console.log(err);
        // });
        
        this.selectedLibro._cod_Libro = this.LibroArray.length > 0 ? this.LibroArray[this.LibroArray.length-1]._cod_Libro+1 : 1;
        this.LibroArray.push(this.selectedLibro);
      }
      else{
        alert("llama Api actualizar");
        // this.httpFactory.postHeaderService('NAME',"").then((resp) => {
        //   debugger;
        //   //Se realiza el llanado de la entidad LibroDTO();
        // }).catch((err) => {
        //   console.log(err);
        // });
      }
      this.selectedLibro = new LibroDTO();
      this.nombreButton = 'Crear';
    }else{
      alert('Porfavor ingresar todos los datos');
    }
  }

   ActualizarLibro(item: LibroDTO){
    this.selectedLibro = item;
    this.nombreButton = 'Actualizar';
  }

  EliminarLibro(item : LibroDTO){
    if(confirm('Estas seguro de elimiar el Libro?'))
    alert("llama Api eliminar");
    this.selectedLibro = item
    this.LibroArray = this.LibroArray.filter(x => x!= this.selectedLibro)
    this.selectedLibro = new LibroDTO();
  }

  ConsultarLibro(name : string){
    
    alert("llama Api consulta por libro");
        // this.httpFactory.postHeaderService('NAME',"").then((resp) => {
        //   debugger;
        //   //Se realiza el llanado de la entidad LibroDTO();
        // }).catch((err) => {
        //   console.log(err);
        // });

        var libroDTO = new LibroDTO();
        libroDTO._cod_Libro = 1;
        libroDTO._nombre_Libro = 'libro1';
        libroDTO._iSBN = 123;
        libroDTO._cod_Autor = 1;
        libroDTO._cod_Categoria = 2;
        this.responseLibroArray.push(libroDTO);
    
        var libroDTO = new LibroDTO();
        libroDTO._cod_Libro = 2;
        libroDTO._nombre_Libro = 'libro2';
        libroDTO._iSBN = 123;
        libroDTO._cod_Autor = 1;
        libroDTO._cod_Categoria = 2;
        this.responseLibroArray.push(libroDTO);

  }

  

}
