import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AutorComponent } from './autor/autor.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { LibroComponent } from './libro/libro.component';
import {FormsModule} from '@angular/forms';
import { NavvarComponent } from './navvar/navvar.component';
@NgModule({
  declarations: [
    AppComponent,
    AutorComponent,
    CategoriaComponent,
    LibroComponent,
    NavvarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
