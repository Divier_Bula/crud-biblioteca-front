import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navvar',
  templateUrl: './navvar.component.html',
  styleUrls: ['./navvar.component.css']
})
export class NavvarComponent implements OnInit {

  constructor(private route : Router) { }

  ngOnInit() {
  }

  router(destinationroute : string)
  {
      this.route.navigate(["/" + destinationroute])
  }

}
